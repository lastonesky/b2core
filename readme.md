使用入门
====
b2Core 目录结构  
----

├── app/ 应用目录  
│   ├── b2core.php b2core 核心文件  
│   ├── config.php b2core 配置文件  
│   ├── c/ 控制器目录  
│   ├── m/ 模块类目录  
│   ├── v/ 模板文件目录  
│   └── lib/ 其他类库目录  
├── index.php 入口文件  
└── .htaccess apache配置文件  
  
###核心类和函数  
  
类: C 控制器  
----
  
所有的web 请求最终都将解析为对控制器类中方法的请求。  
其他控制器可以直接继承 c ，或者完全独立也可。  
未制定控制器时，默认调用 home 类。  
未指定方法时，默认调用 index 方法。  
如访问 http://b2core.b24.cn/, 将调用 app/c/home.php 文件 class home 中的  function index()支持多级目录。  
例如：  
浏览器请求 http://b2core.b24.cn/good/yes/ 将会调用 class good 中的 yes 方法。  
如果存在 app/c/good/ 目录，且存在 app/c/good/__construct.php 文件， 将会调用app/c/good/yes.php 文件中的 class yes  中的function index() 方法。  
  
类:  DB 数据库操作  
----
  
封装了基本的数据库操作。分别如下：  
db::query($sql)  
执行一句SQL 语句，如果有数据返回，则将数据转化为数组并返回。  
db::muti_query($sql)  
执行多句SQL语句。语句之间需要用';' 分隔。  
db::insert_id()  
返回最近一个插入操作生成的 id  
  
类：M 模块  
----
  
所有模块需继承 class m 类。具体方法有：  
m::add( array $arr ) 将 $arr 插入到数据表中  
m::update( int $id, array $arr ) 更新 id 为 $id 的数据 为 $arr  
m::del(int $id) 删除 $id 数据  
m::get() 方法，get 方法有以下两种使用方法：  
- m::get(int $id) , 返回 $id 数据  
- m::get(str $postquery , int $cur = 1,int $psize = 30) 返回符合 $postquery 查询条件，以 $pagesize 条记录为1页的，第 $cur 页的数据  
  
函数: LOAD()  
----

load 函数可以加载 模块类、控制器类、以及类库。 如：  
$school = load('m/school');   
将加载 m/school.php 文件中的 school 模块类，并赋予给 $school 变量。  
$school = load('c/school');   
将加载 c/school.php 文件中的 school 控制器类，并赋予给 $school 变量。  
$school = load('lib/school');   
将加载 lib/school.php 文件中的 school 类库，并赋予给 $school 变量。  
以上方法在加载类的同时实例化类。 也可以仅仅通过 load 函数引用类而不实例化。 方法如下load('lib/school', false);   
将仅仅引用 lib/school 文件；  
你可以可以传递变量到需要调用的类。如：  
$param = array(1,'asdf'); $school = load('lib/school',$param);  
将调用 lib/school.php 中的 school 类的初始化方法， construct(1,'asdf');  
  
函数: VIEW( 模板名，变量数组 , 是否暂不输出)  
----
  
调用 app/v/ 目录下的模板, 如:  
 view('app/school/list',$param);  
将调用 app/v/school/list.php